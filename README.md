# Jai Vulkan SDL

This repo is an example of how to use Vulkan with SDL in Jai.
The code is mostly based on https://vkguide.dev/ with some stuff comming from https://vulkan-tutorial.com/.

The Vulkan module that comes with Jai doesn't support Vulkan 1.3, so I downloaded the latest Vulkan headers and ran the generator on them, which required some tweaks. They can be found in the `modules/jai-vulkan` folder. It is included using Git submodules, the base repo is located here : https://gitlab.com/Stowy/jai-vulkan.

The SDL module doesn't have generated FFI, so to be able to have the Vulkan related procedures, I added `SDL_vulkan.jai` with the two procedures I needed. This only exists in this repo.

## Getting started

Clone using :

```
git clone https://gitlab.com/Stowy/jai-vulkan-sdl.git --recursive
```
